package main;

import java.util.*;

public class MegaSenaApplication {

    public static void main(String[] args) {

        int valor = 0;
        int posicao = 7;
        long[] numApostado = new long[6];

        Scanner entrada = new Scanner(System.in);
        List<Long> listaValores = new ArrayList<Long>();

        System.out.println("----------------\nAposte 6 n�meros\n----------------");

        for(int i = 1; i < posicao; i++) {
            System.out.print(i + ": ");
            numApostado[valor] = entrada.nextLong();
            listaValores.add(numApostado[valor]);
        }
        
        System.out.println("\n-------- Aposta -------\n" + listaValores);

        List<Long> listaRandoms = randomNumbers();
        System.out.println("\n-------- Sorteio --------\n" + listaRandoms);

        System.out.println("\n-------- Resultado --------");

        if (compareNumbers(listaRandoms, listaValores).size() >= 1 && compareNumbers(listaRandoms, listaValores).size() < 6) {
            System.out.println("Voc� acertou: " + compareNumbers(listaRandoms, listaValores) + "\nTotal de acertos: " + compareNumbers(listaRandoms, listaValores).size());
            System.out.println("\nParabens voc� acertou " + compareNumbers(listaRandoms, listaValores).size() + " est� quase l�!!!");

        } else if (compareNumbers(listaRandoms, listaValores).size() == 6) {
            System.out.println("Voc� acertou: " + compareNumbers(listaRandoms, listaValores) + "\nTotal de acertos: " + compareNumbers(listaRandoms, listaValores).size());
            System.out.println("\nParabens voc� acertou TUDO!!!");

        } else {
            System.out.println("N�mero(s): " + 0 + "\nTotal de acertos: " + 0);
            System.out.println("\nQue pena n�o foi dessa vez. Mas continue tentando!!!");
        }
        
        entrada.close();
    }

    public static List<Long> randomNumbers() {

        int valor = 0;
        int posicao = 7;
        long[] numSorteado = new long[6];

        Random aleatorio = new Random();
        List<Long> listaValores = new ArrayList<Long>();

        for(int i = 1; i < posicao; i++) {
            numSorteado[valor] = aleatorio.nextInt(50); // de 0 a 49
            listaValores.add(numSorteado[valor]);
        }

        return listaValores;
    }

    public static List<Long> compareNumbers(List<Long> listaRandoms, List<Long> listaValores) {

        List<Long> compareNumbers = new ArrayList<Long>();

        for (Long random : listaRandoms) {
            for (Long valor : listaValores) {
                if (valor == random) {
                    compareNumbers.add(valor);
                }
               continue;
            }
        }
        
        return compareNumbers;
    }
}